import { AuthStore } from '~/store/AuthStore'

export default function ({ route, redirect }) {
  AuthStore.checkLogin()

  if (!AuthStore.logged) {
    AuthStore.SET_RETURN_URL({ ...route })
    return redirect('/login')
  }
  // Authentication with user levels:
  // const authorizationLevels = route.meta.map((meta) => {
  //   if (meta.auth && typeof meta.auth.levelUser !== 'undefined')
  //     return meta.auth.levelUser;
  //   return 0
  // });
}
