import 'izitoast/dist/css/iziToast.min.css'
import iZtoast from 'izitoast'

const AppToast = {
  error: (message, title = 'Error') => {
    return iZtoast.error({
      title,
      message,
      position: 'bottomCenter'
    })
  },
  success: (message, title = 'Hecho') => {
    return iZtoast.success({
      title,
      message,
      position: 'bottomCenter'
    })
  },
  info: (message, title = 'Info') => {
    return iZtoast.info({
      title,
      message,
      position: 'bottomCenter'

    })
  }
}

export default AppToast
