export interface Proveedor{
  idproveedores?: number,
  nombre?: string,
  dni?:number,
  email?:string,
  telefono?:string,
  web?:string,
  ciudad?: string,
  direccion?: string,
}
