
export interface Cliente {
    idclientes?:number,
    nombre?:string,
    dni?:number,
    telefono?:string,
    direccion?:string,
    email?:string,
  // eslint-disable-next-line camelcase
    created_at?:string,
  // eslint-disable-next-line camelcase
    updated_at?:string,
}
