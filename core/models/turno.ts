/* eslint-disable */
export interface Turno {
  clientes_idclientes?: number
  fecha?: string
  hora_fin?: string
  hora_inicio?: string
  idcalendario?: number
  titulo?: string
  veterinarios_idveterinarios?: number
  mascotas_idmascotas?: number
}
