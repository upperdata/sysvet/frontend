import { UserType } from '~/core/enums/userType'

export interface User {
  id?:number;
  name?:string;
  email?:string;
  password?:string;
  username?:number;
  direccion?:string;
  tipo?:UserType;
  // eslint-disable-next-line camelcase
  last_login?:number;
  // eslint-disable-next-line camelcase
  fecha_nacimiento?:string;
  telefono?:string;
}
