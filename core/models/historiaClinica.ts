/* eslint-disable */
export interface HistoriaClinica {
  idhistorias?:number,
  observaciones?:string,
  mascotas_idmascotas?:number,
  clientes_idclientes?:number,
  veterinarios_idveterinarios?:number,
  tipo?:number | string,
}
