/* eslint-disable */
export interface Mascota {
  idmascotas?:number,
  nombre?:string,
  raza?:string,
  fecha_nacimiento?:string,
  sexo?:string,
  observaciones?:string,
  clientes_idclientes?:number,
}
