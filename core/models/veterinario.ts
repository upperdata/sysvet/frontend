export interface Veterinario{
  idveterinarios?:number
  name?:string
  dni?:number
  email?:string
  // eslint-disable-next-line camelcase
  fecha_nacimiento?:string
  password?:string
  telefono?:string
  tipo?:string
  direccion?:string
  matricula?:string
}
