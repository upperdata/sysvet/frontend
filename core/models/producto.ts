export interface Producto {
  idproductos?:number,
  codigo?:string,
  nombre?:string,
  stock?:number,
  subTotal?:number,
  tipo?:number,
  // eslint-disable-next-line camelcase
  edad_aplicacion?:string,
  // eslint-disable-next-line camelcase
  fecha_vencimiento?:string,
  // eslint-disable-next-line camelcase
  precio_ingreso?:number,
  // eslint-disable-next-line camelcase
  created_at?:string,
  // eslint-disable-next-line camelcase
  updated_at?:string,
  cantidad?: number,
  // eslint-disable-next-line camelcase
  precio_venta?: number,
  // eslint-disable-next-line camelcase
  proveedores_idproveedores?:number,
}
