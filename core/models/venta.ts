export interface Venta {
    idventas?:number,
    total?:string,
  subTotal?: number,
  IVA?: number,
  rinde?: string,
  vuelto?: number,
  // eslint-disable-next-line camelcase
    clientes_idclientes?:number,
    fecha?: string,
  // eslint-disable-next-line camelcase
    numero_factura?: string,
  // eslint-disable-next-line camelcase
    created_at?:string,
  detalle?:any[]
}
