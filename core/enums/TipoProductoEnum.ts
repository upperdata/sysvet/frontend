import { KeyValueInterface } from '@/core/KeyValueInterface'

export enum TipoProductoEnum {
  PRODUCTO = 1,
  VACUNA = 2,
  MEDICAMENTO = 3,
  SERVICIO = 4,
}

export function getTipoProducto (tipo:TipoProductoEnum): KeyValueInterface {
  switch (tipo) {
    case TipoProductoEnum.PRODUCTO: return { id: tipo, label: 'Producto' }
    case TipoProductoEnum.VACUNA: return { id: tipo, label: 'Vacuna' }
    case TipoProductoEnum.MEDICAMENTO: return { id: tipo, label: 'Medicamento' }
    case TipoProductoEnum.SERVICIO: return { id: tipo, label: 'Servicio' }
    default: return { id: null, label: 'Desconocido' }
  }
}
