import { KeyValueInterface } from '@/core/KeyValueInterface'

export enum SexoEnum {
  MASCULINO = 1,
  FEMENINO = 2,
}

export function getSexo (tipo:SexoEnum): KeyValueInterface {
  switch (tipo) {
    case SexoEnum.MASCULINO: return { id: tipo, label: 'Masculino' }
    case SexoEnum.FEMENINO: return { id: tipo, label: 'Femenino' }
    default: return { id: null, label: 'Desconocido' }
  }
}
