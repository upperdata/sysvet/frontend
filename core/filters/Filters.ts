import Vue from 'vue'
import { UserType } from '~/core/enums/userType'
import { EnumsHelpers } from '~/core/helpers/EnumsHelpers'

export default () => {
  Vue.filter('tipodeusuario', function (numero) {
    const t = EnumsHelpers.enumtolist(UserType).find(s => s.id === Number(numero))
    return t ? t.name : 'Desconocido'
  })
}
