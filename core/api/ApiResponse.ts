export interface ApiResponse {
    message?:string;
    data?:any;
    total?:number;
}
