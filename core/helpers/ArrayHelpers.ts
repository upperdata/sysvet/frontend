export abstract class ArrayHelpers {
  public static sortObjectsArray(array:any[], fieldName:string){
    array.sort((a,b) => {
      if(a[fieldName] < b[fieldName]) { return -1; }
      if(a[fieldName] > b[fieldName]) { return 1; }
      return 0;
    });
    return array;
  }

  public static chunk(array, size) {
    if (!array) return [];
    const firstChunk = array.slice(0, size); // create the first chunk of the given array
    if (!firstChunk.length) {
      return array; // this is the base case to terminal the recursive
    }
    return [firstChunk].concat(this.chunk(array.slice(size, array.length), size));
  }
}
