export class EnumsHelpers {
  public static enumtolist (e) {
    const StringIsNumber = value => isNaN(Number(value)) === false
    return Object.keys(e).filter(StringIsNumber)
      .map(key => ({ name: e[key], id: Number(key) }))
  }
}
