import moment from 'moment'
export abstract class DateHelpers {
  public static obtenerTimestamp (date:Date) {
    return date.getTime()
  }

  public static getFechaHoy ():string {
    return moment().format('YYYY-MM-DD')
  }

  public static getFechaHoyHumanFechaFull ():string {
    return moment().format('DD/MM/YYYY HH:mm')
  }

  /**
     * Convierte un objeto Date a string con fecha y hora
     * @param dateObject
     */
  public static dateToString (dateObject:Date) {
    return moment(dateObject).format('YYYY-MM-DD HH:mm:ss')
  }

  /**
     * Devuevle solo la hora y minutos
     * @param date
     */
  public static getHora (date:Date|string):string {
    return moment(date).format('HH:mm:ss')
  }

  /**
     * Devuelve solo la fecha en formato string
     * @param dateObject
     */
  public static getFecha (dateObject:Date):string {
    return moment(dateObject).format('YYYY-MM-DD')
  }

  public static convertHumanFecha (fecha:string) {
    return moment((fecha)).format('DD/MM/YYYY')
  }

  public static convertHumanFechaFull (fecha:string) {
    return moment(fecha).format('DD/MM/YYYY HH:mm')
  }

  public static convertFechaRelativa (fecha:string) {
    return moment(fecha).fromNow()
  }

  public static convertHoras (horas:string) {
    return moment(horas, 'HH:mm:ss').format('HH:mm')
  }

  public static validateHora (hora:string):boolean {
    return moment(`2014-12-13 ${hora}`, 'YYYY-MM-DD HH:mm', true).isValid()
  }

  public static isFuture (date:string):boolean {
    return moment(date).isAfter(moment())
  }
}
