import Vue from 'vue'
import VueRouter from 'vue-router'
import VuetifyConfirm from 'vuetify-confirm'

export default ({ app }) => {
  Vue.use(VueRouter)

  Vue.use(VuetifyConfirm, {
    vuetify: app.vuetify,
    buttonTrueText: 'Si',
    buttonFalseText: 'No',
    color: 'accent',
    width: 350,
    property: '$confirm',
    buttonTrueColor: 'accent'
  })
}
