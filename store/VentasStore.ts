import { Action, getModule, Module, Mutation, VuexModule } from '~/node_modules/vuex-module-decorators'
import { store } from '~/store'
import { App } from '~/core/App'
import { ApiResponse } from '~/core/api/ApiResponse'
import { Venta } from '~/core/models/venta'
import { ClientesStore } from '~/store/ClientesStore'

@Module({ dynamic: true, store, name: 'VentasStore', namespaced: true, stateFactory: true })
class storeClass extends VuexModule {
    // states
    listaVentas:Venta[] = [];
    listaVentasIndexed = {};
    idVentaActual:number = null;
    ventaActual:Venta = null;
    showDialog:boolean = false;
    // mutations
    // @Mutation
    @Mutation
    private SET_LISTA_VENTAS (lista:Venta[]) {
      this.listaVentas = lista
      // eslint-disable-next-line no-return-assign
      lista.forEach(p => this.listaVentasIndexed[p.idventas] = p)
    }

    @Mutation
    private SET_VENTA (venta:Venta) {
      this.ventaActual = venta
    }

    @Mutation
    private ADD_VENTA (venta:Venta) {
      this.listaVentas = [
        ...this.listaVentas.filter(s => s.idventas !== venta.idventas), venta
      ]
    }

    @Mutation
    public MOSTRAR_FORMULARIO_VENTAS (payload:{abrir:boolean, id?:number}) {
      this.showDialog = payload.abrir

      // si se cierra el formulario, quitamos el medico seleccionado
      if (payload.abrir === false) {
        this.idVentaActual = null
      } else if (payload.id > 0) {
        this.idVentaActual = payload.id
      }
    }

    @Mutation
    public ELIMINAR_VENTA (id:number) {
      this.listaVentas = this.listaVentas.filter(
        h => h.idventas !== id
      )
    }
    // @Action

    @Action
    descargarListaVentas ():Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$get<ApiResponse>('/ventas').then((resupuesta) => {
          this.SET_LISTA_VENTAS(resupuesta.data)

          resolve()// no devolvemos nadaa
        }).catch(err => reject(err))
      })
    }

    @Action
    registrarVentas (venta:Venta):Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$post<ApiResponse>('/ventas', venta).then((mensaje) => {
          this.ADD_VENTA(mensaje.data)
          this.SET_VENTA(mensaje.data)
          this.descargarListaVentas()
          ClientesStore.descargarListaClientes()
          resolve(mensaje.data.id)
        }).catch(err => reject(err))
      })
    }

    @Action
    modificarVenta (venta:Venta):Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$put<ApiResponse>('/ventas/' + venta.idventas, venta).then((mensaje) => {
          this.ADD_VENTA(mensaje.data)

          this.MOSTRAR_FORMULARIO_VENTAS({ abrir: false })
          resolve(mensaje.data.id)
        }).catch(err => reject(err))
      })
    }

    @Action
    eliminarVenta (id:number):Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$delete<ApiResponse>('/ventas/' + id).then(() => {
          this.ELIMINAR_VENTA(id)
          resolve()
        }).catch(err => reject(err))
      })
    }
}

export const VentasStore = getModule(storeClass)
