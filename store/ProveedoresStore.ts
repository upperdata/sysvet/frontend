import { Action, getModule, Module, Mutation, VuexModule } from '~/node_modules/vuex-module-decorators'
import { store } from '~/store'
import { App } from '~/core/App'
import { ApiResponse } from '~/core/api/ApiResponse'
import { Venta } from '~/core/models/venta'
import { Proveedor } from '~/core/models/proveedor'

@Module({ dynamic: true, store, name: 'ProveedorsStore', namespaced: true, stateFactory: true })
class storeClass extends VuexModule {
    // states
    listaProveedores:Proveedor[] = [];
    listaProveedoresIndexed = {};
    idProveedorActual:number = null;
    showDialog:boolean = false;
    ventasProveedorActual:Venta[] = [];
    // mutations
    // @Mutation
    @Mutation
    private SET_LISTA_PROVEEDORES (lista:Proveedor[]) {
      this.listaProveedores = lista
      lista.forEach((p) => {
        this.listaProveedoresIndexed[p.idproveedores] = p
      })
    }

    @Mutation
    private SET_PROVEEDOR (ventas:Venta[]) {
      this.ventasProveedorActual = ventas
    }

    @Mutation
    private ADD_PROVEEDOR (proveedor:Proveedor) {
      this.listaProveedores = [
        ...this.listaProveedores.filter(s => s.idproveedores !== proveedor.idproveedores), proveedor
      ]
    }

    @Mutation
    public MOSTRAR_FORMULARIO_PROVEEDORES (payload:{abrir:boolean, id?:number}) {
      this.showDialog = payload.abrir

      // si se cierra el formulario, quitamos el medico seleccionado
      if (payload.abrir === false) {
        this.idProveedorActual = null
      } else if (payload.id > 0) {
        this.idProveedorActual = payload.id
      }
    }

    @Mutation
    public ELIMINAR_PROVEEDOR (id:number) {
      this.listaProveedores = this.listaProveedores.filter(
        h => h.idproveedores !== id
      )
    }
    // @Action

    @Action
    descargarListaProveedores ():Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$get<ApiResponse>('/proveedores').then((resupuesta) => {
          this.SET_LISTA_PROVEEDORES(resupuesta.data)

          resolve()// no devolvemos nadaa
        }).catch(err => reject(err))
      })
    }

    @Action
    registrarProveedores (proveedor:Proveedor):Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$post<ApiResponse>('/proveedores', proveedor).then((mensaje) => {
          this.ADD_PROVEEDOR(mensaje.data)
          this.MOSTRAR_FORMULARIO_PROVEEDORES({ abrir: false })
          resolve(mensaje.data)
        }).catch(err => reject(err))
      })
    }

    @Action
    modificarProveedor (proveedor:Proveedor):Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$put<ApiResponse>('/proveedores/' + proveedor.idproveedores, proveedor).then((mensaje) => {
          this.ADD_PROVEEDOR(mensaje.data)
          this.MOSTRAR_FORMULARIO_PROVEEDORES({ abrir: false })
          resolve(mensaje.data.id)
        }).catch(err => reject(err))
      })
    }

    @Action
    eliminarProveedor (idproveedores:number):Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$delete<ApiResponse>('/proveedores/' + idproveedores).then(() => {
          this.ELIMINAR_PROVEEDOR(idproveedores)
          resolve()
        }).catch(err => reject(err))
      })
    }
}

export const ProveedoresStore = getModule(storeClass)
