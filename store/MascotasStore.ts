import { Action, getModule, Module, Mutation, VuexModule } from '~/node_modules/vuex-module-decorators'
import { store } from '~/store'
import { Mascota } from '~/core/models/mascota'
import { App } from '~/core/App'
import { ApiResponse } from '~/core/api/ApiResponse'

@Module({ dynamic: true, store, name: 'MascotasStore', namespaced: true, stateFactory: true })
class storeClass extends VuexModule {
// states
  mascotas:Mascota[] = [];
  mascotasIndexes = {};
  idMascotaActual:number = null
  showDialog:boolean = false;
  // mutations
  // @Mutation
  @Mutation
  private SET_LISTA_MASCOTAS (lista:Mascota[]) {
    this.mascotas = lista
    lista.forEach((p) => {
      this.mascotasIndexes[p.idmascotas] = p
    })
  }

  @Mutation
  private ADD_MASCOTA (mascota:Mascota) {
    this.mascotas = [
      ...this.mascotas.filter(s => s.idmascotas !== mascota.idmascotas), mascota
    ]
  }

  @Mutation
  public MOSTRAR_FORMULARIO_MASCOTAS (payload:{abrir:boolean, id?:number}) {
    this.showDialog = payload.abrir

    // si se cierra el formulario, quitamos el medico seleccionado
    if (payload.abrir === false) {
      this.idMascotaActual = null
    } else if (payload.id > 0) {
      this.idMascotaActual = payload.id
    }
  }

  @Mutation
  public ELIMINAR_MASCOTA (id:number) {
    this.mascotas = this.mascotas.filter(
      h => h.idmascotas !== id
    )
  }
  // @Action

  @Action
  descargarMascotas ():Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$get<ApiResponse>('/mascotas').then((resupuesta) => {
        this.SET_LISTA_MASCOTAS(resupuesta.data)
        resolve()// no devolvemos nadaa
      }).catch(err => reject(err))
    })
  }

  @Action
  registrarMascota (mascota:Mascota):Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$post<ApiResponse>('/mascotas', mascota).then((mensaje) => {
        this.ADD_MASCOTA(mensaje.data)
        this.MOSTRAR_FORMULARIO_MASCOTAS({ abrir: false })
        resolve(mensaje.data)
      }).catch(err => reject(err))
    })
  }

  @Action
  modificarMascota (mascota:Mascota):Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$put<ApiResponse>('/mascotas/' + mascota.idmascotas, mascota).then((mensaje) => {
        this.ADD_MASCOTA(mensaje.data)
        this.MOSTRAR_FORMULARIO_MASCOTAS({ abrir: false })
        resolve(mensaje.data.idcalendario)
      }).catch(err => reject(err))
    })
  }

  @Action
  eliminarMascota (idmascotas:number):Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$delete<ApiResponse>('/mascotas/' + idmascotas).then(() => {
        this.ELIMINAR_MASCOTA(idmascotas)
        resolve()
      }).catch(err => reject(err))
    })
  }
}

export const MascotasStore = getModule(storeClass)
