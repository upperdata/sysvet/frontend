import { Action, getModule, Module, Mutation, VuexModule } from '~/node_modules/vuex-module-decorators'
import { store } from '~/store'
import { App } from '~/core/App'
import { ApiResponse } from '~/core/api/ApiResponse'
import { Turno } from '~/core/models/turno'

@Module({ dynamic: true, store, name: 'TurnosStore', namespaced: true, stateFactory: true })
class storeClass extends VuexModule {
// states
  turnos:Turno[] = [];
  idTurnoActual:number = null
  showDialog:boolean = false;
  // mutations
  // @Mutation
  @Mutation
  private SET_LISTA_TURNOS (lista:Turno[]) {
    this.turnos = lista
  }

  @Mutation
  private ADD_TURNO (turno:Turno) {
    this.turnos = [
      ...this.turnos.filter(s => s.idcalendario !== turno.idcalendario), turno
    ]
  }

  @Mutation
  public MOSTRAR_FORMULARIO_TURNOS (payload:{abrir:boolean, id?:number}) {
    this.showDialog = payload.abrir

    // si se cierra el formulario, quitamos el medico seleccionado
    if (payload.abrir === false) {
      this.idTurnoActual = null
    } else if (payload.id > 0) {
      this.idTurnoActual = payload.id
    }
  }

  @Mutation
  public ELIMINAR_TURNO (id:number) {
    this.turnos = this.turnos.filter(
      h => h.idcalendario !== id
    )
  }
  // @Action

  @Action
  descargarTurnos ():Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$get<ApiResponse>('/calendarios').then((resupuesta) => {
        this.SET_LISTA_TURNOS(resupuesta.data)
        resolve()// no devolvemos nadaa
      }).catch(err => reject(err))
    })
  }

  @Action
  registrarTurno (turno:Turno):Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$post<ApiResponse>('/calendarios', turno).then((mensaje) => {
        this.ADD_TURNO(mensaje.data)
        this.MOSTRAR_FORMULARIO_TURNOS({ abrir: false })
        resolve(mensaje.data)
      }).catch(err => reject(err))
    })
  }

  @Action
  modificarTurno (turno:Turno):Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$put<ApiResponse>('/calendarios/' + turno.idcalendario, turno).then((mensaje) => {
        this.ADD_TURNO(mensaje.data)
        this.MOSTRAR_FORMULARIO_TURNOS({ abrir: false })
        resolve(mensaje.data.idcalendario)
      }).catch(err => reject(err))
    })
  }

  @Action
  eliminarTurno (idturnos:number):Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$delete<ApiResponse>('/calendarios/' + idturnos).then(() => {
        this.ELIMINAR_TURNO(idturnos)
        resolve()
      }).catch(err => reject(err))
    })
  }
}

export const TurnosStore = getModule(storeClass)
