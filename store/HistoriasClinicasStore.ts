import { Action, getModule, Module, Mutation, VuexModule } from '~/node_modules/vuex-module-decorators'
import { store } from '~/store'
import { App } from '~/core/App'
import { ApiResponse } from '~/core/api/ApiResponse'
import { HistoriaClinica } from '~/core/models/historiaClinica'

@Module({ dynamic: true, store, name: 'HistoriasClinicasStore', namespaced: true, stateFactory: true })
class storeClass extends VuexModule {
// states
  historiasClinicas:HistoriaClinica[] = [];
  idHistoriaClinicaActual:number = null
  showDialog:boolean = false;
  // mutations
  // @Mutation
  @Mutation
  private SET_LISTA_HISTORIA_CLINICAS (lista:HistoriaClinica[]) {
    this.historiasClinicas = lista
  }

  @Mutation
  private ADD_HISTORIA_CLINICA (historiasClinica:HistoriaClinica) {
    this.historiasClinicas = [
      ...this.historiasClinicas.filter(s => s.idhistorias !== historiasClinica.idhistorias), historiasClinica
    ]
  }

  @Mutation
  public MOSTRAR_FORMULARIO_HISTORIA_CLINICAS (payload:{abrir:boolean, id?:number}) {
    this.showDialog = payload.abrir

    // si se cierra el formulario, quitamos el medico seleccionado
    if (payload.abrir === false) {
      this.idHistoriaClinicaActual = null
    } else if (payload.id > 0) {
      this.idHistoriaClinicaActual = payload.id
    }
  }

  @Mutation
  public ELIMINAR_HISTORIA_CLINICA (id:number) {
    this.historiasClinicas = this.historiasClinicas.filter(
      h => h.idhistorias !== id
    )
  }
  // @Action

  @Action
  descargarHistoriaClinicas ():Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$get<ApiResponse>('/historias').then((resupuesta) => {
        this.SET_LISTA_HISTORIA_CLINICAS(resupuesta.data)
        resolve()// no devolvemos nadaa
      }).catch(err => reject(err))
    })
  }

  @Action
  registrarHistoriaClinica (historiasClinica:HistoriaClinica):Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$post<ApiResponse>('/historias', historiasClinica).then((mensaje) => {
        this.ADD_HISTORIA_CLINICA(mensaje.data)
        this.MOSTRAR_FORMULARIO_HISTORIA_CLINICAS({ abrir: false })
        resolve(mensaje.data)
      }).catch(err => reject(err))
    })
  }

  @Action
  modificarHistoriaClinica (historiasClinica:HistoriaClinica):Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$put<ApiResponse>('/historias/' + historiasClinica.idhistorias, historiasClinica).then((mensaje) => {
        this.ADD_HISTORIA_CLINICA(mensaje.data)
        this.MOSTRAR_FORMULARIO_HISTORIA_CLINICAS({ abrir: false })
        resolve(mensaje.data.idcalendario)
      }).catch(err => reject(err))
    })
  }

  @Action
  eliminarHistoriaClinica (idhistoriasClinicas:number):Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$delete<ApiResponse>('/historias/' + idhistoriasClinicas).then(() => {
        this.ELIMINAR_HISTORIA_CLINICA(idhistoriasClinicas)
        resolve()
      }).catch(err => reject(err))
    })
  }
}

export const HistoriasClinicasStore = getModule(storeClass)
