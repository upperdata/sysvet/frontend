import { Action, getModule, Module, Mutation, VuexModule } from '~/node_modules/vuex-module-decorators'
import { store } from '~/store'
import { Producto } from '~/core/models/producto'
import { App } from '~/core/App'
import { ApiResponse } from '~/core/api/ApiResponse'

@Module({ dynamic: true, store, name: 'ProductosStore', namespaced: true, stateFactory: true })
class storeClass extends VuexModule {
    // states
    listaProductos:Producto[] = [];
    listaProductosIndexed = {};
    idProductoActual:number = null;
    showDialog:boolean = false;
    // mutations
    // @Mutation
    @Mutation
    private SET_LISTA_PRODUCTOS (lista:Producto[]) {
      this.listaProductos = lista
      lista.forEach(p => this.listaProductosIndexed[p.idproductos] = p)
    }

    @Mutation
    private ADD_PRODUCTO (producto:Producto) {
      this.listaProductos = [
        ...this.listaProductos.filter(s => s.idproductos !== producto.idproductos), producto
      ]
    }

    @Mutation
    public MOSTRAR_FORMULARIO_PRODUCTOS (payload:{abrir:boolean, id?:number}) {
      this.showDialog = payload.abrir

      // si se cierra el formulario, quitamos el medico seleccionado
      if (payload.abrir === false) {
        this.idProductoActual = null
      } else if (payload.id > 0) {
        this.idProductoActual = payload.id
      }
    }

    @Mutation
    public ELIMINAR_PRODUCTO (id:number) {
      this.listaProductos = this.listaProductos.filter(
        h => h.idproductos !== id
      )
    }
    // @Action

    @Action
    descargarListaProductos ():Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$get<ApiResponse>('/productos').then((resupuesta) => {
          this.SET_LISTA_PRODUCTOS(resupuesta.data)
          resolve()// no devolvemos nadaa
        }).catch(err => reject(err))
      })
    }

    @Action
    registrarProductos (producto:Producto):Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$post<ApiResponse>('/productos', producto).then((mensaje) => {
          this.ADD_PRODUCTO(mensaje.data)
          this.MOSTRAR_FORMULARIO_PRODUCTOS({ abrir: false })
          resolve(mensaje.data.idproductos)
        }).catch(err => reject(err))
      })
    }

    @Action
    modificarProducto (producto:Producto):Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$put<ApiResponse>('/productos/' + producto.idproductos, producto).then((mensaje) => {
          this.ADD_PRODUCTO(mensaje.data)
          this.MOSTRAR_FORMULARIO_PRODUCTOS({ abrir: false })
          resolve(mensaje.data.idproductos)
        }).catch(err => reject(err))
      })
    }

    @Action
    eliminarProducto (id:number):Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$delete<ApiResponse>('/productos/' + id).then(() => {
          this.ELIMINAR_PRODUCTO(id)
          resolve()
        }).catch(err => reject(err))
      })
    }
}

export const ProductosStore = getModule(storeClass)
