import { Action, getModule, Module, Mutation, VuexModule } from '~/node_modules/vuex-module-decorators'
import { store } from '~/store'
import { Veterinario } from '~/core/models/veterinario'
import { App } from '~/core/App'
import { ApiResponse } from '~/core/api/ApiResponse'

@Module({ dynamic: true, store, name: 'VeterinariosStore', namespaced: true, stateFactory: true })
class storeClass extends VuexModule {
  /// ////////////////////////////////////////////////////////VARIABLES////////////////////////////////////////////////////////

  listaVeterinarios:Veterinario[] = [];
  listaVeterinariosIndexed = {}
  showDialog:boolean=false;
  idVeterinarioActual:number=null;

  /// ////////////////////////////////////////////////////////MUTATIONS////////////////////////////////////////////////////////

  @Mutation
  private SET_LISTA_VETERINARIOS (lista:Veterinario[]) {
    this.listaVeterinarios = lista
    lista.forEach((p) => {
      this.listaVeterinariosIndexed[p.idveterinarios] = p
    })
  }

  @Mutation
  private ADD_VETERINARIO (veterinario:Veterinario) {
    this.listaVeterinarios = [
      ...this.listaVeterinarios.filter(o => o.idveterinarios !== veterinario.idveterinarios), veterinario
    ]
  }

  @Mutation
  public MOSTRAR_FORMULARIO_VETERINARIOS (payload:{abrir:boolean, id?:number}) {
    this.showDialog = payload.abrir

    if (payload.abrir === false) {
      this.idVeterinarioActual = null
    } else if (payload.id > 0) {
      this.idVeterinarioActual = payload.id
    }
  }

  @Mutation
  public ELIMINAR_VETERINARIO (id:number) {
    this.listaVeterinarios = this.listaVeterinarios.filter(
      h => h.idveterinarios !== id
    )
  }

  /// /////////////////////////////////////////////////////////ACTIONS////////////////////////////////////////////////////////

  @Action
  descargarListaVeterinarios ():Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$get<ApiResponse>('/veterinarios').then((resupuesta) => {
        this.SET_LISTA_VETERINARIOS(resupuesta.data)

        resolve()// no devolvemos nadaa
      }).catch(e => reject(e))
    })
  }

  @Action
  registrarVeterinario (veterinario:Veterinario):Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$post<ApiResponse>('/veterinarios', veterinario).then((mensaje) => {
        this.ADD_VETERINARIO(mensaje.data)
        this.MOSTRAR_FORMULARIO_VETERINARIOS({ abrir: false })
        resolve(mensaje.data)
      }).catch(e => reject(e))
    })
  }

  @Action
  modificarVeterinario (veterinario:Veterinario):Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$put<ApiResponse>('/veterinarios/' + veterinario.idveterinarios, veterinario).then((mensaje) => {
        this.ADD_VETERINARIO(mensaje.data)
        this.MOSTRAR_FORMULARIO_VETERINARIOS({ abrir: false })

        resolve()
      }).catch(err => reject(err))
    })
  }

  @Action
  eliminarVeterinario (id:number):Promise<void> {
    return new Promise<void>((resolve, reject) => {
      App.$axios.$delete<ApiResponse>('/veterinarios/' + id).then(() => {
        this.ELIMINAR_VETERINARIO(id)
        resolve()
      }).catch(err => reject(err))
    })
  }
}

export const VeterinariosStore = getModule(storeClass)
