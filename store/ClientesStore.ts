import { Action, getModule, Module, Mutation, VuexModule } from '~/node_modules/vuex-module-decorators'
import { store } from '~/store'
import { App } from '~/core/App'
import { ApiResponse } from '~/core/api/ApiResponse'
import { Venta } from '~/core/models/venta'
import { Cliente } from '~/core/models/cliente'

@Module({ dynamic: true, store, name: 'ClientesStore', namespaced: true, stateFactory: true })
class storeClass extends VuexModule {
    // states
    listaClientes:Cliente[] = [];
    listaClientesIndexed = {};
    idClienteActual:number = null;
    showDialog:boolean = false;
    ventasClienteActual:Venta[] = [];
    // mutations
    // @Mutation
    @Mutation
    private SET_LISTA_CLIENTES (lista:Cliente[]) {
      this.listaClientes = lista
      lista.forEach((p) => {
        this.listaClientesIndexed[p.idclientes] = p
      })
    }

    @Mutation
    private SET_CLIENTE (ventas:Venta[]) {
      this.ventasClienteActual = ventas
    }

    @Mutation
    private ADD_CLIENTE (cliente:Cliente) {
      this.listaClientes = [
        ...this.listaClientes.filter(s => s.idclientes !== cliente.idclientes), cliente
      ]
    }

    @Mutation
    public MOSTRAR_FORMULARIO_CLIENTES (payload:{abrir:boolean, id?:number}) {
      this.showDialog = payload.abrir

      // si se cierra el formulario, quitamos el medico seleccionado
      if (payload.abrir === false) {
        this.idClienteActual = null
      } else if (payload.id > 0) {
        this.idClienteActual = payload.id
      }
    }

    @Mutation
    public ELIMINAR_CLIENTE (id:number) {
      this.listaClientes = this.listaClientes.filter(
        h => h.idclientes !== id
      )
    }
    // @Action

    @Action
    descargarListaClientes ():Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$get<ApiResponse>('/clientes').then((resupuesta) => {
          this.SET_LISTA_CLIENTES(resupuesta.data)

          resolve()// no devolvemos nadaa
        }).catch(err => reject(err))
      })
    }

    @Action
    descargarCliente (cliente:Cliente):Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$get<ApiResponse>('/clientes/' + cliente.idclientes + '/historiaclinica').then((resupuesta) => {
          this.SET_CLIENTE(resupuesta.data)

          resolve()// no devolvemos nadaa
        }).catch(err => reject(err))
      })
    }

    @Action
    registrarClientes (cliente:Cliente):Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$post<ApiResponse>('/clientes', cliente).then((mensaje) => {
          this.ADD_CLIENTE(mensaje.data)
          this.descargarListaClientes()
          this.MOSTRAR_FORMULARIO_CLIENTES({ abrir: false })
          resolve(mensaje.data)
        }).catch(err => reject(err))
      })
    }

    @Action
    modificarCliente (cliente:Cliente):Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$put<ApiResponse>('/clientes/' + cliente.idclientes, cliente).then((mensaje) => {
          this.ADD_CLIENTE(mensaje.data)
          this.MOSTRAR_FORMULARIO_CLIENTES({ abrir: false })
          resolve(mensaje.data.id)
        }).catch(err => reject(err))
      })
    }

    @Action
    eliminarCliente (idclientes:number):Promise<void> {
      return new Promise<void>((resolve, reject) => {
        App.$axios.$delete<ApiResponse>('/clientes/' + idclientes).then(() => {
          this.ELIMINAR_CLIENTE(idclientes)
          resolve()
        }).catch(err => reject(err))
      })
    }
}

export const ClientesStore = getModule(storeClass)
