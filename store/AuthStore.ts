import Vue from 'vue'
import JwtDecode from 'jwt-decode'
import {
  Action, getModule, Module, Mutation, VuexModule
} from '~/node_modules/vuex-module-decorators'
import { store } from '~/store/index'
import { Route } from '~/node_modules/vue-router'
import { User } from '~/core/models/user'
import { App } from '~/core/App'
import { ApiResponse } from '~/core/api/ApiResponse'

import { UserType } from '~/core/enums/userType'

const ST_TOKEN_KEY = 'user_token'
const ST_USER_KEY = 'user'

export interface TokenPayload {
  aud: string;
  jti: string;
  iat: number;
  nbf: number;
  exp: number;
  sub: string;
  scopes: any[];
}

export enum LoginErrors {
  DATOS_INCORRECTOS,
  ERROR_AL_OBTENER_DATOS,
  ERROR_RED
}

export interface LoginResponse {
  // eslint-disable-next-line camelcase
  access_token: string;
  tokenType: string;
  expiresAt: string;
}

@Module({
  dynamic: true,
  store,
  name: 'auth',
  namespaced: true,
  stateFactory: true
})
class authStore extends VuexModule {
  // states
  // variables que se pueden acceder desde cualquier lugar pero de solo lectura.
  // userToken:string = Vue.$cookies.get(ST_TOKEN_KEY)
  userToken:string = null

  user: User = null;
  /** *
   * Indica si el usuario esta logeado
   */
   logged: boolean = false
   // logged: boolean = !!Vue.$cookies.get(ST_TOKEN_KEY)

  /**
   * Es la url a la que se deve voler
   */
  returnUrl: Route = null;

  loginError:boolean | LoginErrors = false;

  get isAdmin ():boolean {
    return this.logged && this.user && this.user.tipo === UserType.ADMIN
  }

  get isVet ():boolean {
    return this.logged && this.user && this.user.tipo === UserType.VETERINARIO
  }

  @Mutation
  public SET_RETURN_URL (route: Route | null) {
    this.returnUrl = route
  }

  @Mutation
  private SET_USER () {
    // guarda el token
    // const token = sessionStorage.getItem(ST_TOKEN_KEY)
    const token = Vue.$cookies.get(ST_TOKEN_KEY)
    if (token !== null) {
      try {
        const payload:TokenPayload = JwtDecode(token)
        if (payload) {
          // guarda el user en json
          // const userjson = sessionStorage.getItem(ST_USER_KEY)
          const userjson = Vue.$cookies.get(ST_USER_KEY)

          // trasforma el formato del user
          // const userdata:any = JSON.parse(userjson)
          this.user = userjson.user

          this.logged = true
          this.userToken = token
          return // RETURN ON OK
        }
      } catch (e) {
      }
    }
    this.user = null
    this.logged = false
    this.userToken = null
  }

  @Mutation
  public SET_LOGGED (val:boolean) {
    this.logged = val
  }

  @Mutation
  private SET_LOGIN_ERROR (err:boolean|LoginErrors) {
    this.loginError = err
  }

  @Action
  public login ({ user, password }) {
    // eslint-disable-next-line no-async-promise-executor
    return new Promise<User>(async (resolve, reject) => {
      // Real action
      // Desactivamos el mensaje de error al hacer el login

      const config:any = { errorHandler: false }
      try {
        const res1 = await App.$axios.$post<LoginResponse>('/auth/login', { user, password }, config)
        console.log(res1)
        const token = res1.access_token
        Vue.$cookies.set(ST_TOKEN_KEY, token)
        // sessionStorage.setItem(ST_TOKEN_KEY, token)
        const res2 = await App.$axios.$get<ApiResponse>('/users/me', {
          headers: { Authorization: `Bearer ${token}` }
        })
        const user2 = res2.data
        // trasforma el objeto a tipo json
        const json = JSON.stringify(user2)
        Vue.$cookies.set(ST_USER_KEY, json)
        // sessionStorage.setItem(ST_USER_KEY, json)
        this.SET_USER()
        resolve(user2)
      } catch (e) {
        reject(e)
      }
    })
  }

  /** *
   * Elimina los datos de la sesion.
   * NO REDIRECCIONA
   */
  @Action
  public logout () {
    // sessionStorage.clear()
    Vue.$cookies.keys().forEach(cookie => Vue.$cookies.remove(cookie))
    this.SET_USER()
  }

  @Action
  public checkLogin () {
    this.SET_USER()
  }
}

export const AuthStore = getModule(authStore)
